#!/bin/bash

# 1 XOR Problem
## Neural network solution
cd python # go to python folder
python XorNet.py
cd .. # go back to the root folder of the project
## GP solution
cd java # go to java folder
java -cp Project2-java.jar ec.Evolve -file ecj_params/xor/xor.params
cd .. # go back to the root folder of the project

# 2 Digit Recognition
## neural network solution
### task digit00, digit15, digit30 and digit60
cd python
./digit_rec.sh
## KNN solution
### task digit00, digit15, digit30 and digit60
./digit_rec_knn.sh
cd ..

# 3 Symbolic Regression Problem
cd java
java -cp Project2-java.jar ec.Evolve -file ecj_params/srp/srp.params
cd ..

# 4 Function Optimisation
cd java
# Rosenbrock’s function with 20 dimension
java -cp Project2-java.jar ec.Evolve -file ecj_params/pso/rosenbrock_d20.params
# Rosenbrock’s function with 50 dimension
java -cp Project2-java.jar ec.Evolve -file ecj_params/pso/rosenbrock_d50.params
# Griewanks’s function with 20 dimension
java -cp Project2-java.jar ec.Evolve -file ecj_params/pso/griewanks_d20.params
# Griewanks’s function with 50 dimension
java -cp Project2-java.jar ec.Evolve -file ecj_params/pso/griewanks_d50.params
cd ..

# 5 Feature Construction
cd java
# split train test sets
java -cp Project2-java.jar FeatureConstruction split
# construct feature for balance scale data using wrapper with naive bayes
java -cp Project2-java.jar ec.Evolve -file ecj_params/fc/balance_naivebayes.params
# construct feature for balance scale data using wrapper with decision tree
java -cp Project2-java.jar ec.Evolve -file ecj_params/fc/balance_decisiontree.params
# construct feature for wine recognition data using wrapper with naive bayes
java -cp Project2-java.jar ec.Evolve -file ecj_params/fc/wine_naivebayes.params
# construct feature for wine recognition data using wrapper with decision tree
java -cp Project2-java.jar ec.Evolve -file ecj_params/fc/wine_decisiontree.params
# train and test the classifier
java -cp Project2-java.jar FeatureConstruction
cd ..

# 6 Feature Selection
cd java
# using train-test
java -cp Project2-java.jar FeatureSelection
# using cross-validation
java -cp Project2-java.jar FeatureSelectionCV
cd ..

# 7 Data Processing for Classiﬁcation
# It is more clear and intuitive to look at 
# the jupyter notebook DataProcessing.ipynb if you have the environment
# Otherwise, the python script below can be run as well
python DataProcessing.py