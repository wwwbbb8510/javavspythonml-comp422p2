# description

The project can be split to two parts - one part is developed by java which is mainly for GP and Feature Selection and the other part is developed by python which is to solve the neural network problems. 

The main java frameworks are ECJ which is for GP related problems and Weka which is for feature selection and java classification. 

The main python frameworks is tensorflow which is specilized in Neural Network

# dependencies

## java dependencies

The java program is developed in Java 8, the external libraries are ECJ related libraries which can be found on ECJ official site and the weka library. I've also uploaded all of the libraries in a zip file named java-libs.zip. 

## python dependencies
The python program is developed in Python 3 under Anaconda environment with the libraries of tensorflow, pandas, sklearn, numpy, matplotlib etc. If there is any libraries missing when running the code, please use pip to install them. 

# How to run the code? 

There are two ways to run the code - load the source code into IDE and run it from there which is recommended way, and run the jar file or the python code directly which also works. 

## run the source code in IDE

### the code structure

Unzip the source code and put the java libraries and datasets into the source directory by following the structure below: 

Project2
	- java
		- src
			- comp422
			- libs
				- [jar files extracted from java-libs.zip]
			- FeatureConstruction.java
			- FeatureSelection.java
	- python
		- [python source code files]
	- Project2-dataset
		- digits
		- uci-datasets

### run java code 

Following the steps below to create a project from the source code. 

1. Open IDE - IntelliJ is recommended
2. Create a java application project using the java code in the fold Project2/java/src
3. Add all jar files in Project/java/src/libs
4. Create enty points for each of the problems

```
1 XOR Problem(GP solution): Main class: ec.Evolve, Program arguments: -file src/comp422/xor/xor.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

3 Symbolic Regression Problem: Main class: ec.Evolve, Program arguments: -file src/comp422/srp/srp.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

4 Function Optimisation - Rosenbrock’s function with 20 dimension: Main class: ec.Evolve, Program arguments: -file src/comp422/pso/rosenbrock_d20.params , Working directory: C:\code\exercises\COMP422\Project2\java

4 Function Optimisation - Rosenbrock’s function with 50 dimension: Main class: ec.Evolve, Program arguments: -file src/comp422/pso/rosenbrock_d50.params, Working directory: C:\code\exercises\COMP422\Project2\java

4 Function Optimisation - Griewanks’s function with 20 dimension: Main class: ec.Evolve, Program arguments: -file src/comp422/pso/griewanks_d20.params, Working directory: C:\code\exercises\COMP422\Project2\java

4 Function Optimisation - Griewanks’s function with 50 dimension: Main class: ec.Evolve, Program arguments: -file src/comp422/pso/griewanks_d50.params, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - Split dataset: Main class: FeatureConstruction, Program arguments: split, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - Balance Scale with Naive Bayes: Main class: ec.Evolve, Program arguments: -file src/comp422/fc/balance_naivebayes.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - Balance Scale with Naive Bayes: Main class: ec.Evolve, Program arguments: -file src/comp422/fc/balance_naivebayes.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - Balance Scale with Naive Bayes: Main class: ec.Evolve, Program arguments: -file src/comp422/fc/balance_naivebayes.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - Balance Scale with Decision Tree: Main class: ec.Evolve, Program arguments: -file src/comp422/fc/balance_decisiontree.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - Wine Recognition with Naive Bayes: Main class: ec.Evolve, Program arguments: -file src/comp422/fc/wine_naivebayes.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - Wine Recognition with Decision Tree: Main class: ec.Evolve, Program arguments: -file src/comp422/fc/wine_decisiontree.params  -p gp.tree.print-style=dot, Working directory: C:\code\exercises\COMP422\Project2\java

5 Feature Construction - train and test the classifier: Main class: FeatureConstruction, Working directory: C:\code\exercises\COMP422\Project2\java

6 Feature Selection - using train test: Main class: FeatureSelection, Working directory: C:\code\exercises\COMP422\Project2\java

6 Feature Selection - using cross validation: Main class: FeatureSelectionCV, Working directory: C:\code\exercises\COMP422\Project2\java
``` 

### run python code 

1. Open IDE - Pycharm is recommended
2. Configure Python version to pyton 3 under anaconda
3. Run python scripts as following

```
1 XOR Problem - Neural Network: XorNet.py

2 Digit Recognition - Neural Network one task: DigitRecognition.py (passing 0,3,5 or 8 as the parameter to specify the task)

2 Digit Recognition - KNN one task: DigitRecognitionKNN.py (passing 0,3,5 or 8 as the parameter to specify the task)

2 Digit Recognition - Neural Network all task: digit_rec.sh

2 Digit Recognition - KNN all task: digit_rec_knn.sh
```

4. Run jupyter notebook

```
In jupyter notebook, open DataProcessing.ipynb
```

## run jar file and python commands

Following the code structure at the beginning of running the code in IDE

Download the jar file and unzip the ecj_params.zip, and put them into Porject2/java folder as the following structure. 
- Project2
	- java
		- Project2-java.jar
		- ecj_params
			- common
			- fc
			- pso
			- srp
			- xor

All of the code can be run at once by running the SCRIPTS.sh or run each of them by copying the corresponding command from SCRIPTS.sh