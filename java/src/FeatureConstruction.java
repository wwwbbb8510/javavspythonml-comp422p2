import comp422.fc.FitnessCalculator;
import comp422.weka.TrainClassifier;
import weka.classifiers.Evaluation;
import weka.core.*;
import weka.core.converters.ConverterUtils;

import java.util.ArrayList;
import java.util.Map;

public class FeatureConstruction {
    public static void main(String[] args) throws Exception {
        String balanceDataPath = FitnessCalculator.BALANCE_TEST_DATA_PATH;
        String wineDataPath = FitnessCalculator.WINE_TEST_DATA_PATH;
        if (args.length == 2){
            balanceDataPath = args[0];
            wineDataPath = args[1];
        }

        //load data
        Instances balanceData = null;
        Instances wineData = null;

        if(args.length == 1 && "split".equals(args[0])){
            //load data
            balanceData = TrainClassifier.loadData(FitnessCalculator.BALANCE_DATA_PATH);
            wineData = TrainClassifier.loadData(FitnessCalculator.WINE_DATA_PATH);
            //split training and test set
            Map<String, Instances> splitedBalanceData = TrainClassifier.splitTrainTest(balanceData);
            Map<String, Instances> splitedWineData = TrainClassifier.splitTrainTest(wineData);
            Instances balanceTrainingData = splitedBalanceData.get("train");
            Instances balanceTestData = splitedBalanceData.get("test");
            Instances wineTrainingData = splitedWineData.get("train");
            Instances wineTestData = splitedWineData.get("test");
            ConverterUtils.DataSink.write(FitnessCalculator.BALANCE_TRAIN_DATA_PATH, balanceTrainingData);
            ConverterUtils.DataSink.write(FitnessCalculator.BALANCE_TEST_DATA_PATH, balanceTestData);
            ConverterUtils.DataSink.write(FitnessCalculator.WINE_TRAIN_DATA_PATH, wineTrainingData);
            ConverterUtils.DataSink.write(FitnessCalculator.WINE_TEST_DATA_PATH, wineTestData);
            System.exit(0);
        }else{
            //load data
            balanceData = TrainClassifier.loadData(FitnessCalculator.BALANCE_TEST_DATA_PATH);
            wineData = TrainClassifier.loadData(FitnessCalculator.WINE_TEST_DATA_PATH);
        }

        //Naive Bayes on original data
        Evaluation balanceNaiveBayesEval = TrainClassifier.trainNaiveBayes(balanceData);
        Evaluation wineNaiveBayesEval = TrainClassifier.trainNaiveBayes(wineData);
        outputSummary(balanceNaiveBayesEval, "Naive Bayes performance on original Balance data");
        outputSummary(wineNaiveBayesEval, "Naive Bayes performance on original Wine data");

        //Decision tree on original data
        Evaluation balanceDecisionTreeEval = TrainClassifier.trainDecisionTree(balanceData);
        Evaluation wineDecisionTreeEval = TrainClassifier.trainDecisionTree(wineData);
        outputSummary(balanceDecisionTreeEval, "Decision tree performance on original Balance data");
        outputSummary(wineDecisionTreeEval, "Decision tree performance on original Wine data");

        //construct features
        Instances cBalanceDataNaivebayes = TrainClassifier.loadData(FitnessCalculator.BALANCE_CONSTRUCTED_NAIVEBAYES_DATA_PATH);
        Instances cBalanceDataDecisiontree = TrainClassifier.loadData(FitnessCalculator.BALANCE_CONSTRUCTED_DECISIONTREE_DATA_PATH);
        Instances cWineDataNaivebayes = TrainClassifier.loadData(FitnessCalculator.WINE_CONSTRUCTED_NAIVEBAYES_DATA_PATH);
        Instances cWineDataDecisiontree = TrainClassifier.loadData(FitnessCalculator.WINE_CONSTRUCTED_DECISIONTREE_DATA_PATH);

        //Naive Bayes on constructed data
        balanceNaiveBayesEval = TrainClassifier.trainNaiveBayes(cBalanceDataNaivebayes);
        wineNaiveBayesEval = TrainClassifier.trainNaiveBayes(cWineDataNaivebayes);
        outputSummary(balanceNaiveBayesEval, "Naive Bayes performance on constructed Balance data");
        outputSummary(wineNaiveBayesEval, "Naive Bayes performance on constructed Wine data");

        //decision tree on constructed data
        balanceDecisionTreeEval = TrainClassifier.trainNaiveBayes(cBalanceDataDecisiontree);
        wineDecisionTreeEval = TrainClassifier.trainNaiveBayes(cWineDataDecisiontree);
        outputSummary(balanceDecisionTreeEval, "Decision tree performance on constructed Balance data");
        outputSummary(wineDecisionTreeEval, "Decision tree performance on constructed Wine data");
    }

    protected static void outputSummary(Evaluation eval, String title){
        System.out.println(eval.toSummaryString("\n " + title + " Results\n======\n", false));
    }
}
