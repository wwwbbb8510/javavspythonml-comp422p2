package comp422.pso;

import ec.EvolutionState;
import ec.pso.Particle;
import ec.util.Parameter;

/**
 * extend the Particle class to add velocity clamp
 */
public class ParticleVClamped extends Particle{
    /**
     * the parameter name of the max velocity set in the params file
     */
    public static final String P_VELOCITY_MAX = "max_velocity" ;
    /**
     * store the max velocity
     */
    public double maxVelocity;

    public void setup(final EvolutionState state, final Parameter base)
    {
        super.setup(state, base);
        velocity = new double[genome.length];
        // read the max velocity from the params file
        maxVelocity = state.parameters.getDouble(base.push(P_VELOCITY_MAX),null,0.0);
    }

    // velocityCoeff:       cognitive/confidence coefficient for the velocity
    // personalCoeff:       cognitive/confidence coefficient for self
    // informantCoeff:      cognitive/confidence coefficient for informants/neighbours
    // globalCoeff:         cognitive/confidence coefficient for global best, this is not done in the standard PSO
    public void tweak(
            EvolutionState state,  double[] globalBest,
            double velocityCoeff, double personalCoeff,
            double informantCoeff, double globalCoeff,
            int thread)
    {
        for(int x = 0 ; x < genomeLength() ; x++)
        {
            double xCurrent = genome[x] ;
            double xPersonal = personalBestGenome[x] ;
            double xNeighbour = neighborhoodBestGenome[x] ;
            double xGlobal = globalBest[x] ;
            double beta = state.random[thread].nextDouble() * personalCoeff ;
            double gamma = state.random[thread].nextDouble() * informantCoeff ;
            double delta = state.random[thread].nextDouble() * globalCoeff ;

            double newVelocity = (velocityCoeff * velocity[x]) + (beta * (xPersonal - xCurrent)) + (gamma * (xNeighbour - xCurrent)) + (delta * (xGlobal - xCurrent)) ;
            // use the max velocity if the velocity is greater than the max
            if(maxVelocity > 0 && maxVelocity < Math.abs(newVelocity)){
                newVelocity = newVelocity > 0 ? maxVelocity : -maxVelocity;
            }
            velocity[x] = newVelocity ;
            genome[x] += newVelocity ;
        }

        evaluated = false ;

        // printIndividual(state, 0);

    }
}
