package comp422.pso;

import ec.EvolutionState;
import ec.Individual;
import ec.Problem;
import ec.simple.SimpleFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorSpecies;

/**
 * Rosenbrock symbolic regression problem
 */
public class RosenbrockProblem extends Problem implements SimpleProblemForm {
    public static final double[] range = new double[]{-30, 30};

    boolean alreadyChecked = false;

    public void checkRange(EvolutionState state, double[] genome) {
        if (alreadyChecked || state.generation > 0) return;
        alreadyChecked = true;
        for (int i = 0; i < state.population.subpops.size(); i++) {
            if (!(state.population.subpops.get(i).species instanceof FloatVectorSpecies)) {
                state.output.fatal("ECSuite requires species " + i + " to be a FloatVectorSpecies, but it is a: " + state.population.subpops.get(i).species);
            }
            FloatVectorSpecies species = (FloatVectorSpecies) (state.population.subpops.get(i).species);
            for (int k = 0; k < genome.length; k++) {
                if (species.minGene(k) != range[0] ||
                        species.maxGene(k) != range[1]) {
                    state.output.warning("Gene range is nonstandard for the problem \nFirst occurrence: Subpopulation " + i + " Gene " + k +
                            " range was [" + species.minGene(k) + ", " + species.maxGene(k) +
                            "], expected [" + range[0] + ", " + range[1] + "]");
                    return;  // done here
                }
            }
        }
    }

    public void evaluate(final EvolutionState state,
                         final Individual ind,
                         final int subpopulation,
                         final int threadnum) {
        if (ind.evaluated)  // don't bother reevaluating
            return;

        if (!(ind instanceof DoubleVectorIndividual))
            state.output.fatal("The individuals for this problem should be DoubleVectorIndividuals.");

        DoubleVectorIndividual temp = (DoubleVectorIndividual) ind;
        double[] genome = temp.genome;
        //int len = genome.length;

        // this curious break-out makes it easy to use the isOptimal() and function() methods
        // for other purposes, such as coevolutionary versions of this class.

        // compute the fitness on a per-function basis
        //convert minimization to maximization
        double fit = -(function(state, temp.genome, threadnum));

        // compute if we're optimal on a per-function basis
        boolean isOptimal = isOptimal(fit, state);

        if (fit < (0.0 - Double.MAX_VALUE))  // uh oh -- can be caused by Product for example
        {
            ((SimpleFitness)(ind.fitness)).setFitness( state, 0.0 - Double.MAX_VALUE, isOptimal );
            state.output.warnOnce("'Product' type used: some fitnesses are negative infinity, setting to lowest legal negative number.");
        }
        else if (fit > Double.MAX_VALUE)  // uh oh -- can be caused by Product for example
        {
            ((SimpleFitness)(ind.fitness)).setFitness( state, Double.MAX_VALUE, isOptimal );
            state.output.warnOnce("'Product' type used: some fitnesses are negative infinity, setting to lowest legal negative number.");
        }
        else
        {
            ((SimpleFitness)(ind.fitness)).setFitness( state, fit, isOptimal );
        }
        ind.evaluated = true;
    }

    /**
     * symbolic regression function - Rosenbrock
     * @param state
     * @param genome
     * @param threadnum
     * @return
     */
    public double function(EvolutionState state, double[] genome, int threadnum)
    {
        checkRange(state, genome);
        double value = 0;
        double len = genome.length;
        for( int i = 1 ; i < len ; i++ )
        {
            double gj = genome[i-1] ;
            double gi = genome[i] ;
            value += 100 * (gi - gj*gj) * (gi - gj*gj) + (gj - 1) * (gj - 1);
        }
        return value;
    }
    public boolean isOptimal(double fitness, final EvolutionState state){
        if (fitness == 0){
            return true;
        }
        return false;
    }
}