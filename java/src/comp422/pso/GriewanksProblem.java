package comp422.pso;

import ec.EvolutionState;
import ec.Individual;
import ec.Problem;
import ec.simple.SimpleFitness;
import ec.simple.SimpleProblemForm;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorSpecies;

/**
 * Griewanks symbolic regression problem
 */
public class GriewanksProblem  extends Problem implements SimpleProblemForm {
    public static final double[] range = new double[]{-30, 30};

    boolean alreadyChecked = false;

    public void checkRange(EvolutionState state, double[] genome) {
        if (alreadyChecked || state.generation > 0) return;
        alreadyChecked = true;
        for (int i = 0; i < state.population.subpops.size(); i++) {
            if (!(state.population.subpops.get(i).species instanceof FloatVectorSpecies)) {
                state.output.fatal("ECSuite requires species " + i + " to be a FloatVectorSpecies, but it is a: " + state.population.subpops.get(i).species);
            }
            FloatVectorSpecies species = (FloatVectorSpecies) (state.population.subpops.get(i).species);
            for (int k = 0; k < genome.length; k++) {
                if (species.minGene(k) != range[0] ||
                        species.maxGene(k) != range[1]) {
                    state.output.warning("Gene range is nonstandard for the problem \nFirst occurrence: Subpopulation " + i + " Gene " + k +
                            " range was [" + species.minGene(k) + ", " + species.maxGene(k) +
                            "], expected [" + range[0] + ", " + range[1] + "]");
                    return;  // done here
                }
            }
        }
    }

    public void evaluate(final EvolutionState state,
                         final Individual ind,
                         final int subpopulation,
                         final int threadnum) {
        if (ind.evaluated)  // don't bother reevaluating
            return;

        if (!(ind instanceof DoubleVectorIndividual))
            state.output.fatal("The individuals for this problem should be DoubleVectorIndividuals.");

        DoubleVectorIndividual temp = (DoubleVectorIndividual) ind;
        double[] genome = temp.genome;
        //int len = genome.length;

        // this curious break-out makes it easy to use the isOptimal() and function() methods
        // for other purposes, such as coevolutionary versions of this class.

        // compute the fitness on a per-function basis
        // convert minimization to maximization
        double fit = -(function(state, temp.genome, threadnum));

        // compute if we're optimal on a per-function basis
        boolean isOptimal = isOptimal(fit);

        if (fit < (0.0 - Double.MAX_VALUE))  // uh oh -- can be caused by Product for example
        {
            ((SimpleFitness)(ind.fitness)).setFitness( state, 0.0 - Double.MAX_VALUE, isOptimal );
            state.output.warnOnce("'Product' type used: some fitnesses are negative infinity, setting to lowest legal negative number.");
        }
        else if (fit > Double.MAX_VALUE)  // uh oh -- can be caused by Product for example
        {
            ((SimpleFitness)(ind.fitness)).setFitness( state, Double.MAX_VALUE, isOptimal );
            state.output.warnOnce("'Product' type used: some fitnesses are negative infinity, setting to lowest legal negative number.");
        }
        else
        {
            ((SimpleFitness)(ind.fitness)).setFitness( state, fit, isOptimal );
        }
        ind.evaluated = true;
    }

    /**
     * symbolic regression function - Griewanks
     * @param state
     * @param genome
     * @param threadnum
     * @return
     */
    public double function(EvolutionState state, double[] genome, int threadnum)
    {
        checkRange(state, genome);
        double value = 1;
        double len = genome.length;
        double prod = 1;
        for( int i = 0 ; i < len ; i++ )
        {
            double gi = genome[i] ;
            value += (gi*gi)/4000.0;
            prod *= Math.cos( gi / Math.sqrt(i+1) );
        }
        value -= prod;
        return value;
    }
    public boolean isOptimal(double fitness){
        return false;
    }
}
