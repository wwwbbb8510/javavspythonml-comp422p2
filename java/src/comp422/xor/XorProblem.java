package comp422.xor;

import ec.util.*;
import ec.*;
import ec.gp.*;
import ec.gp.koza.*;
import ec.simple.*;
import java.lang.*;
import comp422.common.*;

/**
 * define the XOR GP problem
 */
public class XorProblem extends GPProblem implements SimpleProblemForm{
    public static final String P_DATA = "data";
    /**
     * store the value for terminal X
     */
    public boolean currentX;
    /**
     * store the value for terminal Y
     */
    public boolean currentY;

    private static final boolean[] arrBool = {true, false};

    public void setup(final EvolutionState state,
                      final Parameter base)
    {
        // very important, remember this
        super.setup(state,base);

        // verify our input is the right class (or subclasses from it)
        if (!(input instanceof BooleanData))
            state.output.fatal("GPData class must subclass from " + BooleanData.class,
                    base.push(P_DATA), null);
    }

    public void evaluate(final EvolutionState state,
                         final Individual ind,
                         final int subpopulation,
                         final int threadnum)
    {
        if (!ind.evaluated)  // don't bother reevaluating
        {
            BooleanData input = (BooleanData)(this.input);

            int hits = 0;// the hits of correct predictions
            double sum = 0.0; //the sum of wrong predictions
            boolean expectedResult;

            //generate all possible X, Y and calculate the sum and hits
            for (boolean xValue: arrBool){
                for (boolean yValue: arrBool){
                    currentX = xValue;
                    currentY = yValue;
                    expectedResult = currentX ^ currentY;
                    ((GPIndividual)ind).trees[0].child.eval(
                            state,threadnum,input,stack,((GPIndividual)ind),this);
                    if(input.x == expectedResult) hits++; else sum++;
                }
            }
            // the fitness better be KozaFitness!
            KozaFitness f = ((KozaFitness)ind.fitness);
            f.setStandardizedFitness(state, sum);
            f.hits = hits;
            ind.evaluated = true;
        }
    }
}
