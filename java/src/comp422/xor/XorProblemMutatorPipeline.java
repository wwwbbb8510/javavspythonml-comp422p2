package comp422.xor;

import ec.vector.*;
import ec.*;
import ec.util.*;

import java.util.ArrayList;
import java.util.HashMap;

public class XorProblemMutatorPipeline{
    //used only for our default base
    public static final String P_OURMUTATION = "our-mutation";

    // We have to specify a default base, even though we never use it
    public Parameter defaultBase() { return VectorDefaults.base().push(P_OURMUTATION); }

    public static final int NUM_SOURCES = 1;

    // Return 1 -- we only use one source
    public int numSources() { return NUM_SOURCES; }


}
