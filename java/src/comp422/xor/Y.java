package comp422.xor;

import ec.*;
import ec.gp.*;
import ec.util.*;
import comp422.common.*;

/**
 * Y terminal
 */
public class Y extends GPNode  {
    public String toString() { return "y"; }

    public int expectedChildren() { return 0; }

    public void eval(final EvolutionState state,
                     final int thread,
                     final GPData input,
                     final ADFStack stack,
                     final GPIndividual individual,
                     final Problem problem)
    {
        BooleanData rd = ((BooleanData)(input));
        rd.x = ((XorProblem)problem).currentY;
    }
}
