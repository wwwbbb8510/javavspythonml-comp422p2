package comp422.common;

import ec.util.*;
import ec.*;
import ec.gp.*;

public class BooleanData  extends GPData {
    public boolean x;    // return value

    public void copyTo(final GPData gpd)   // copy my stuff to another DoubleData
    { ((BooleanData)gpd).x = x; }
}
