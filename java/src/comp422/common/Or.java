package comp422.common;

import ec.*;
import ec.gp.*;
import ec.util.*;

public class Or extends GPNode {
    public String toString() { return "or"; }
    public int expectedChildren() { return 2; }
    public void eval(final EvolutionState state,
                     final int thread,
                     final GPData input,
                     final ADFStack stack,
                     final GPIndividual individual,
                     final Problem problem)
    {
        boolean result;
        BooleanData rd = ((BooleanData)(input));

        children[0].eval(state,thread,input,stack,individual,problem);
        result = rd.x;

        children[1].eval(state,thread,input,stack,individual,problem);
        rd.x = result || rd.x;
    }
}
