package comp422.common;

import comp422.fc.FeatureConstructionProblem;
import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ec.util.Parameter;

import java.util.Random;

public class DoubleFeature extends GPNode {
    public final static String P_FEATURE_AMOUNT = "feature_amount";
    public final static String P_FEATURE_NUM = "feature_num";
    protected int featureNum = -1;
    protected int featureAmount = 4;

    public void setup(final EvolutionState state, final Parameter base)
    {
        //this.featureAmount = state.parameters.getInt(base.push(P_FEATURE_AMOUNT),null,0);
        this.featureNum = state.parameters.getInt(base.push(P_FEATURE_NUM),null,0);
        super.setup(state, base);
    }

    public String toString() {
        this.generateFeatureNum();
        return "Feature" + this.featureNum;
    }

    public int expectedChildren() { return 0; }

    public void eval(final EvolutionState state,
                     final int thread,
                     final GPData input,
                     final ADFStack stack,
                     final GPIndividual individual,
                     final Problem problem)
    {
        //this.generateFeatureNum();
        DoubleData rd = ((DoubleData)(input));
        double[] arrFeatures = ((FeatureConstructionProblem)problem).arrFeatures;
        rd.x = arrFeatures[this.featureNum-1];
    }
    protected int generateFeatureNum(){
        if(this.featureNum == -1){
            this.featureNum = new Random().nextInt(this.featureAmount) + 1;
        }
        return this.featureNum;
    }
}
