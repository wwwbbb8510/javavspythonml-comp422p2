package comp422.srp;

import ec.*;
import ec.gp.*;
import ec.util.*;
import comp422.common.*;

public class X extends GPNode{
    public String toString() { return "x"; }

    public int expectedChildren() { return 0; }

    public void eval(final EvolutionState state,
                     final int thread,
                     final GPData input,
                     final ADFStack stack,
                     final GPIndividual individual,
                     final Problem problem)
    {
        DoubleData rd = ((DoubleData)(input));
        rd.x = ((SymbolicRegressionProblem)problem).currentX;
    }
}
