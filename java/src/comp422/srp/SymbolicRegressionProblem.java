package comp422.srp;

import ec.util.*;
import ec.*;
import ec.gp.*;
import ec.gp.koza.*;
import ec.simple.*;
import java.lang.*;
import java.util.Map;

import comp422.common.*;

/**
 * symbolic regression problem
 */
public class SymbolicRegressionProblem extends GPProblem implements SimpleProblemForm{
    public static final String P_DATA = "data";
    /**
     * store the X value
     */
    public double currentX;

    public void setup(final EvolutionState state,
                      final Parameter base)
    {
        // very important, remember this
        super.setup(state,base);

        // verify our input is the right class (or subclasses from it)
        if (!(input instanceof DoubleData))
            state.output.fatal("GPData class must subclass from " + DoubleData.class,
                    base.push(P_DATA), null);
    }

    public void evaluate(final EvolutionState state,
                         final Individual ind,
                         final int subpopulation,
                         final int threadnum)
    {
        if (!ind.evaluated)  // don't bother reevaluating
        {
            DoubleData input = (DoubleData)(this.input);

            int hits = 0;
            double expectedResult;
            double meanSquaredError = 0;
            /**
             * get the fitness cases
             */
            Map fitnessCases = FitnessCases.generateFitnessCases();
            double[] xData = (double[])fitnessCases.get("x");
            double[] yData = (double[])fitnessCases.get("y");
            /**
             * calculate the MSE which will be used as the fitness value
             */
            for (int i=0;i<xData.length;i++)
            {
                currentX = xData[i];
                expectedResult = yData[i];
                ((GPIndividual)ind).trees[0].child.eval(
                        state,threadnum,input,stack,((GPIndividual)ind),this);
                if(Math.abs(expectedResult - input.x) < 0.01){
                    hits++;
                }
                meanSquaredError += (expectedResult - input.x)*(expectedResult - input.x);
            }
            meanSquaredError /= xData.length;

            //stop when MSE is smaller than 0.5
            if (meanSquaredError < 0.5){
                System.out.println("Final MSE: " + meanSquaredError);
                meanSquaredError = 0;
            }

            // KozaFitness!
            KozaFitness f = ((KozaFitness)ind.fitness);
            f.setStandardizedFitness(state, meanSquaredError);
            f.hits = hits;
            ind.evaluated = true;
        }
    }
}
