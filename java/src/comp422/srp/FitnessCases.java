package comp422.srp;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * generate fitness cases
 */
public class FitnessCases {
    public final static int FITNESS_CASE_BOUND = 10; // the bound of cases
    public final static int FITNESS_CASE_AMOUNT = 100; // case number
    public static double[] xData = null;
    public static double[] yData = null;

    /**
     * generate fitness cases
     * @return
     */
    public static Map<String, double[]> generateFitnessCases(){
        if (xData == null && yData == null){
            xData = new double[FITNESS_CASE_AMOUNT];
            yData = new double[FITNESS_CASE_AMOUNT];
            Random rnd = new Random();
            for (int i=0;i<FITNESS_CASE_AMOUNT;i++)
            {
                double currentX = (rnd.nextDouble() - 0.5) * 2 * FITNESS_CASE_BOUND;
                double expectedResult = 0;
                if(currentX > 0){
                    expectedResult = 1/currentX + Math.sin(currentX);
                }else{
                    expectedResult = 2*currentX + currentX * currentX + 3.0;
                }
                xData[i] = currentX;
                yData[i] = expectedResult;
            }
        }
        Map fitnessCases = new HashMap();
        fitnessCases.put("x", xData);
        fitnessCases.put("y", yData);
        return fitnessCases;
    }
}
