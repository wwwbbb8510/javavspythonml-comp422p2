package comp422.srp;

import ec.*;
import ec.gp.*;
import comp422.common.*;

import java.util.Random;

/**
 * random constant
 */
public class C  extends GPNode{
    protected int randInt = -1;
    protected final static int RAND_BOUND = 10;
    public String toString() { return String.valueOf(this.generateRandInt()); }

    public int expectedChildren() { return 0; }

    public void eval(final EvolutionState state,
                     final int thread,
                     final GPData input,
                     final ADFStack stack,
                     final GPIndividual individual,
                     final Problem problem)
    {
        DoubleData rd = ((DoubleData)(input));
        rd.x = this.generateRandInt();
    }

    protected int generateRandInt(){
        if(this.randInt == -1){
            this.randInt = new Random().nextInt(RAND_BOUND) + 1;
        }
        return this.randInt;
    }
}
