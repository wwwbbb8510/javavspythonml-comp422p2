package comp422.weka;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.core.*;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemovePercentage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * classification training methods
 * including NaiveBayes, Decision Tree using 10-fold cross validation
 */
public class TrainClassifier {
    /**
     * load data
     * @param dataPath
     * @return
     * @throws Exception
     */
    public static Instances loadData(String dataPath) throws Exception {
        //load data set
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(dataPath);
        Instances data = source.getDataSet();
        data.setClassIndex(data.numAttributes() - 1);
        return data;
    }

    /**
     * train Naive Bayes classifier given weka instances
     * @param data
     * @return
     * @throws Exception
     */
    public static Evaluation trainNaiveBayes(Instances data) throws Exception {
        //build classifier
        NaiveBayes classifier = new NaiveBayes();
        //classifier.buildClassifier(data);

        //evaluation
        Evaluation eval = new Evaluation(data);
        eval.crossValidateModel(classifier, data, 10, new Random(1));
        //eval.evaluateModel(classifier, data);
        return eval;
    }

    /**
     * train Naive Bayes classifier given X double array and Y double array
     * @param xTrain
     * @param yTrain
     * @return
     * @throws Exception
     */
    public static Evaluation trainNaiveBayes(double[] xTrain, double[] yTrain) throws Exception {
        Instances trainingSet = buildWekaData(xTrain, yTrain);
        Evaluation eval = trainNaiveBayes(trainingSet);
        return eval;
    }

    /**
     * train decision tree given weka instances
     * @param data
     * @return
     * @throws Exception
     */
    public static Evaluation trainDecisionTree(Instances data) throws Exception {
        //build classifier
        J48 classifier = new J48();
        //classifier.buildClassifier(data);

        //evaluation
        Evaluation eval = new Evaluation(data);
        eval.crossValidateModel(classifier, data, 10, new Random(1));
        //eval.evaluateModel(classifier, data);
        return eval;
    }

    /**
     * train decision tree given X double array and Y double array
     * @param xTrain
     * @param yTrain
     * @return
     * @throws Exception
     */
    public static Evaluation trainDecisionTree(double[] xTrain, double[] yTrain) throws Exception {
        Instances trainingSet = buildWekaData(xTrain, yTrain);
        Evaluation eval = trainDecisionTree(trainingSet);
        return eval;
    }

    /**
     * build weka instances given X double array and Y double array
     * @param xTrain
     * @param yTrain
     * @return
     */
    public static Instances buildWekaData(double[] xTrain, double[] yTrain){
        Attribute featureAttribute = new Attribute("feature");
        ArrayList<String> lables = new ArrayList<>();
        lables.add("1");
        lables.add("2");
        lables.add("3");
        Attribute classAttribute = new Attribute("class", lables);
        FastVector fvAttributes = new FastVector(2);
        fvAttributes.addElement(featureAttribute);
        fvAttributes.addElement(classAttribute);
        Instances trainingSet = new Instances("contructed", fvAttributes, xTrain.length);
        trainingSet.setClassIndex(trainingSet.numAttributes() - 1);
        for (int i=0; i<xTrain.length; i++){
            Instance ins = new DenseInstance(2);
            ins.setValue((Attribute)fvAttributes.elementAt(0), xTrain[i]);
            ins.setValue((Attribute)fvAttributes.elementAt(1), yTrain[i]);
            trainingSet.add(ins);
        }
        return trainingSet;
    }

    /**
     * split train test sets
     * @param data
     * @return
     * @throws Exception
     */
    public static Map<String, Instances> splitTrainTest(Instances data) throws Exception {
        RemovePercentage rp = new RemovePercentage();
        rp.setPercentage(50);
        rp.setInputFormat(data);
        rp.setInvertSelection(true);
        Instances trainingSet = Filter.useFilter(data, rp);
        rp = new RemovePercentage();
        rp.setPercentage(50);
        rp.setInputFormat(data);
        rp.setInvertSelection(false);
        Instances testSet = Filter.useFilter(data, rp);
        Map<String, Instances> splitedSets = new HashMap();
        splitedSets.put("train", trainingSet);
        splitedSets.put("test", testSet);
        return splitedSets;
    }
}
