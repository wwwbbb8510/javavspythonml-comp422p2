package comp422.weka;

import weka.attributeSelection.*;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.AttributeSelectedClassifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;
import weka.classifiers.bayes.NaiveBayes;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.util.Random;

/**
 * feature selection
 */
public class FeatureSelection {

    /**
     * select features by information gain
     * @param data
     * @param amount
     * @return
     * @throws Exception
     */
    public static Instances selectFeaturesByInformationGain(Instances data, int amount, Instances testData) throws Exception {
        AttributeSelection attSel = new AttributeSelection();
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        Ranker search = new Ranker();
        search.setNumToSelect(amount);
        attSel.setEvaluator(eval);
        attSel.setSearch(search);

        // obtain the attribute indices that were selected
        attSel.SelectAttributes(data);
        int[] indices = attSel.selectedAttributes();
        System.out.println(Utils.arrayToString(indices));

        Instances selectedData = filterDataWithSelectedAttributeIndice(testData, indices);

        return selectedData;
    }

    /**
     * using 10-fold CV and Information Gain to perform feature selection
     * @param data
     * @param amount
     * @return
     * @throws Exception
     */
    public static Evaluation attributeSelectedClassifierWithInfoGain(Instances data, int amount) throws Exception {
        //add evaluator and search method
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        Ranker search = new Ranker();
        search.setNumToSelect(amount);

        //add classifier
        NaiveBayes classifier = new NaiveBayes();

        //create AttributeSelectedClassifier
        Evaluation asEval = createAttributeSelectedClassifier(data, eval, search, classifier);

        return asEval;
    }

    /**
     * create AttributeSelectedClassifier using 10-fold CV
     * @param data
     * @param eval
     * @param search
     * @param cls
     * @return
     * @throws Exception
     */
    public static Evaluation createAttributeSelectedClassifier(Instances data, ASEvaluation eval, ASSearch search, Classifier cls) throws Exception {
        //create AttributeSelectedClassifier
        AttributeSelectedClassifier asClassifier = new AttributeSelectedClassifier();
        asClassifier.setEvaluator(eval);
        asClassifier.setSearch(search);
        cls.buildClassifier(data);
        asClassifier.setClassifier(cls);
        asClassifier.buildClassifier(data);
        Evaluation asEval = new Evaluation(data);
        asEval.crossValidateModel(asClassifier, data, 10, new Random(1));
        System.out.println(asClassifier.toString());
        return asEval;
    }

    /**
     * select features by correlation
     * @param data
     * @param amount
     * @return
     * @throws Exception
     */
    public static Instances selectFeaturesByCorrelation(Instances data, int amount, Instances testData) throws Exception {
        AttributeSelection attSel = new AttributeSelection();
        CorrelationAttributeEval eval = new CorrelationAttributeEval();
        Ranker search = new Ranker();
        search.setNumToSelect(amount);
        attSel.setEvaluator(eval);
        attSel.setSearch(search);

        // obtain the attribute indices that were selected
        attSel.SelectAttributes(data);
        int[] indices = attSel.selectedAttributes();
        System.out.println(Utils.arrayToString(indices));

        Instances selectedData = filterDataWithSelectedAttributeIndice(testData, indices);

        return selectedData;
    }

    /**
     * using 10-fold CV and Correlation to perform feature selection
     * @param data
     * @param amount
     * @return
     * @throws Exception
     */
    public static Evaluation attributeSelectedClassifierWithCorrelation(Instances data, int amount) throws Exception {
        //add evaluator and search method
        CorrelationAttributeEval eval = new CorrelationAttributeEval();
        Ranker search = new Ranker();
        search.setNumToSelect(amount);

        //add classifier
        NaiveBayes classifier = new NaiveBayes();

        //create AttributeSelectedClassifier
        Evaluation asEval = createAttributeSelectedClassifier(data, eval, search, classifier);

        return asEval;
    }

    /**
     * select features by wrapper with Naive Bayes
     * @param data
     * @param amount
     * @return
     * @throws Exception
     */
    public static Instances selectFeaturesByWrapperWithNaiveBayes(Instances data, int amount, Instances testData) throws Exception {
        AttributeSelection attSel = new AttributeSelection();
        WrapperSubsetEval eval = new WrapperSubsetEval();
        NaiveBayes classifier = new NaiveBayes();
        //classifier.buildClassifier(data);
        eval.setClassifier(classifier);
        GreedyStepwise search = new GreedyStepwise();
        search.setNumToSelect(amount);
        attSel.setEvaluator(eval);
        attSel.setSearch(search);

        // obtain the attribute indices that were selected
        attSel.SelectAttributes(data);
        int[] indices = attSel.selectedAttributes();
        System.out.println(Utils.arrayToString(indices));

        Instances selectedData = filterDataWithSelectedAttributeIndice(testData, indices);

        return selectedData;
    }

    /**
     * using 10-fold CV and wrapper with Naive Bayes to perform feature selection
     * @param data
     * @param amount
     * @return
     * @throws Exception
     */
    public static Evaluation attributeSelectedClassifierWithWrapperNaiveBayes(Instances data, int amount) throws Exception {
        //add evaluator and search method
        WrapperSubsetEval eval = new WrapperSubsetEval();
        NaiveBayes evClassifier = new NaiveBayes();
        evClassifier.buildClassifier(data);
        eval.setClassifier(evClassifier);
        GreedyStepwise search = new GreedyStepwise();
        search.setNumToSelect(amount);

        //add classifier
        NaiveBayes classifier = new NaiveBayes();

        //create AttributeSelectedClassifier
        Evaluation asEval = createAttributeSelectedClassifier(data, eval, search, classifier);
        return asEval;
    }

    /**
     * filter data with selected features
     * @param data
     * @param indices
     * @return
     * @throws Exception
     */
    protected static Instances filterDataWithSelectedAttributeIndice(Instances data, int[] indices) throws Exception {
        Remove remove = new Remove();
        remove.setAttributeIndicesArray(indices);
        remove.setInvertSelection(true);
        remove.setInputFormat(data);
        return Filter.useFilter(data, remove);
    }
}
