package comp422.fc;

import ec.EvolutionState;
import ec.Individual;
import ec.simple.SimpleFinisher;
import ec.simple.SimpleStatistics;
import ec.util.Parameter;

/**
 * Feature construction finisher
 */
public class FeatureConstructionFinisher extends SimpleFinisher{
    public void setup(final EvolutionState state, final Parameter base) {
        return;
    }
    public void finishPopulation(final EvolutionState state, final int result)
    {
        Individual ind;
        ind = ((SimpleStatistics)state.statistics).getBestSoFar()[0];
        try {
            //save the transformed test data by the best individual
            ((FeatureConstructionProblem)state.evaluator.p_problem).saveData(state, ind, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
