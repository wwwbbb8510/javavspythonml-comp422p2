package comp422.fc;

import comp422.weka.TrainClassifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.util.HashMap;

/**
 * calculate fitness and some common methods used by GP feature construction
 */
public class FitnessCalculator {
    /**
     * data paths
     */
    protected static final String BASE_DATA_FOLDER = "../";
    public static final String BALANCE_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/balance.names";
    public static final String WINE_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/wine.names";
    public static final String BALANCE_TRAIN_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/balance_train.arff";
    public static final String WINE_TRAIN_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/wine_train.arff";
    public static final String BALANCE_TEST_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/balance_test.arff";
    public static final String WINE_TEST_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/wine_test.arff";
    public static final String BALANCE_CONSTRUCTED_NAIVEBAYES_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/balance_constructed_naivebayes.arff";
    public static final String BALANCE_CONSTRUCTED_DECISIONTREE_DATA_PATH = BASE_DATA_FOLDER + "Project2-dataset/uci-datasets/balance_constructed_decisiontree.arff";
    public static final String WINE_CONSTRUCTED_NAIVEBAYES_DATA_PATH = BASE_DATA_FOLDER +  "Project2-dataset/uci-datasets/wine_constructed_naivebayes.arff";
    public static final String WINE_CONSTRUCTED_DECISIONTREE_DATA_PATH = BASE_DATA_FOLDER +  "Project2-dataset/uci-datasets/wine_constructed_decisiontree.arff";

    /**
     * store the loaded data
     */
    protected static HashMap<String, Instances> loadedData = new HashMap<>();

    /**
     * load balance scale data
     * @param source
     * @return
     * @throws Exception
     */
    public static Instances loadBalanceData(String source) throws Exception {
        source = source == null ? "train" : source;
        if (loadedData.get(source) == null){
            System.out.println("Balance dataset is using");
            Instances sBalanceData;
            if (source == "train"){
                sBalanceData = TrainClassifier.loadData(BALANCE_TRAIN_DATA_PATH);
            }else{
                sBalanceData = TrainClassifier.loadData(BALANCE_TEST_DATA_PATH);
            }
            loadedData.put(source, sBalanceData);
        }
        return loadedData.get(source);
    }

    /**
     * load wine recognition data
     * @param source
     * @return
     * @throws Exception
     */
    public static Instances loadWineData(String source) throws Exception {
        source = source == null ? "train" : source;
        if (loadedData.get(source) == null){
            Instances sWineData;
            if(source == "train"){
                sWineData = TrainClassifier.loadData(WINE_TRAIN_DATA_PATH);
            }else{
                sWineData = TrainClassifier.loadData(WINE_TEST_DATA_PATH);
            }
            loadedData.put(source, sWineData);
            System.out.println("Wine dataset is using");
        }
        return loadedData.get(source);
    }

    /**
     * calculate the fitness
     * @param xValues
     * @param yValues
     * @param classifierName
     * @return
     */
    public static double calculateFitness(double[] xValues, double[] yValues, String classifierName){
        Evaluation eval = null;
        double errorRate = 0;
        try{
            if(classifierName.equals("naivebayes") ){
                eval = TrainClassifier.trainNaiveBayes(xValues, yValues);
            }else if(classifierName.equals("decisiontree")){
                eval = TrainClassifier.trainDecisionTree(xValues, yValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        errorRate = eval != null ? eval.errorRate() : 0;
        return errorRate;
    }
}
