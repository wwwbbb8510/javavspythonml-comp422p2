package comp422.fc;

import comp422.common.DoubleData;
import comp422.weka.TrainClassifier;
import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.gp.koza.KozaFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Feature construction problem using GP
 */
public class FeatureConstructionProblem  extends GPProblem implements SimpleProblemForm {
    public static final String P_DATA = "data";
    public static final String P_DATASET = "dataset";
    public static final String P_CLASSIFIER = "classifier";

    public double[] arrFeatures;
    public String datasetName;
    public String classifierName;

    public void setup(final EvolutionState state,
                      final Parameter base)
    {
        // very important, remember this
        super.setup(state,base);

        // read the dataset and classifer from the params file
        this.datasetName = state.parameters.getString(base.push(P_DATASET),null);
        this.classifierName = state.parameters.getString(base.push(P_CLASSIFIER),null);

        // verify our input is the right class (or subclasses from it)
        if (!(input instanceof DoubleData))
            state.output.fatal("GPData class must subclass from " + DoubleData.class,
                    base.push(P_DATA), null);
    }

    public void evaluate(final EvolutionState state,
                         final Individual ind,
                         final int subpopulation,
                         final int threadnum)
    {
        if (!ind.evaluated)  // don't bother reevaluating
        {
            int hits = 0;
            double fitness = 0;
            //transform the data by GP tree
            Map<String, double[]> tData = this.tranformData(state, ind, threadnum, null);
            double[] xValues = tData.get("xValues");
            double[] yValues = tData.get("yValues");
            //perform wrapper feature construction
            //calculate the fitness of the individual which will be the error rate
            fitness = FitnessCalculator.calculateFitness(xValues, yValues, this.classifierName);

            // KozaFitness!
            KozaFitness f = ((KozaFitness)ind.fitness);
            f.setStandardizedFitness(state, fitness);
            f.hits = hits;
            ind.evaluated = true;
        }
    }

    /**
     * save the transformed test data into Weka data file
     * which will be used by the classification later
     * @param state
     * @param ind
     * @param threadnum
     * @throws Exception
     */
    public void saveData(final EvolutionState state, final Individual ind, final int threadnum) throws Exception {
        Map<String, double[]> tData = tranformData(state, ind, threadnum, "test");;
        double[] xValues = tData.get("xValues");
        double[] yValues = tData.get("yValues");
        Instances constructedData = TrainClassifier.buildWekaData(xValues, yValues);
        if("balance".equals(this.datasetName) && "naivebayes".equals(this.classifierName)){
            ConverterUtils.DataSink.write(FitnessCalculator.BALANCE_CONSTRUCTED_NAIVEBAYES_DATA_PATH, constructedData);
        }else if("balance".equals(this.datasetName) && "decisiontree".equals(this.classifierName)){
            ConverterUtils.DataSink.write(FitnessCalculator.BALANCE_CONSTRUCTED_DECISIONTREE_DATA_PATH, constructedData);
        }else if("wine".equals(this.datasetName) && "naivebayes".equals(this.classifierName)){
            ConverterUtils.DataSink.write(FitnessCalculator.WINE_CONSTRUCTED_NAIVEBAYES_DATA_PATH, constructedData);
        }else if("wine".equals(this.datasetName) && "decisiontree".equals(this.classifierName)){
            ConverterUtils.DataSink.write(FitnessCalculator.WINE_CONSTRUCTED_DECISIONTREE_DATA_PATH, constructedData);
        }

    }

    /**
     * transform the data by GP tree
     * @param state
     * @param ind
     * @param threadnum
     * @param source
     * @return
     */
    public Map<String, double[]> tranformData(final EvolutionState state, final Individual ind, final int threadnum, final String source){
        Instances data = null;
        DoubleData input = (DoubleData)(this.input);
        try {
            if (this.datasetName.equals("balance")){
                data = FitnessCalculator.loadBalanceData(source);
            }else if (this.datasetName.equals("wine")){
                data = FitnessCalculator.loadWineData(source);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        double[] xValues = new double[data.numInstances()];
        double[] yValues = new double[data.numInstances()];
        for (int i=0;i<data.numInstances();i++)
        {
            Instance row = data.instance(i);
            double[] arrRowValues = row.toDoubleArray();
            this.arrFeatures = Arrays.copyOfRange(arrRowValues, 0, arrRowValues.length-1);
            ((GPIndividual)ind).trees[0].child.eval(
                    state,threadnum,input,stack,((GPIndividual)ind),this);
            xValues[i] = input.x;
            yValues[i] = arrRowValues[arrRowValues.length-1];
        }
        Map<String, double[]> transformedData = new HashMap<>();
        transformedData.put("xValues", xValues);
        transformedData.put("yValues", yValues);
        return transformedData;
    }

}
