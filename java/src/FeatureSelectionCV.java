import comp422.weka.TrainClassifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.util.Map;

/**
 * feature selection using 10-fold CV
 */
public class FeatureSelectionCV {
    /**
     * data paths
     */
    private static final String WBC_DATA_PATH = "../Project2-dataset/uci-datasets/wbcd.names";
    private static final String SONAR_DATA_PATH = "../Project2-dataset/uci-datasets/sonar.names";
    /**
     * selected feature number
     */
    private static final int FEATURE_NUMBER = 5;

    public static void main(String[] args) throws Exception {
        String wbcDataPath = WBC_DATA_PATH;
        String sonarDataPath = SONAR_DATA_PATH;
        int n = FEATURE_NUMBER;
        if (args.length == 2){
            wbcDataPath = args[0];
            sonarDataPath = args[1];
        }
        if(args.length == 3){
            n = Integer.parseInt(args[2]);
        }
        //load data
        Instances wbcDataWhole = TrainClassifier.loadData(wbcDataPath);
        Instances sonarDataWhole = TrainClassifier.loadData(sonarDataPath);

        //classifier using all features
        Evaluation wbcEval = TrainClassifier.trainNaiveBayes(wbcDataWhole);
        Evaluation sonarEval = TrainClassifier.trainNaiveBayes(sonarDataWhole);
        outputSummary(wbcEval, "WBC All features");
        outputSummary(sonarEval, "Sonar All features");

        //classifier using selected features obtained by Information gain
        Evaluation wbcInfoEval = comp422.weka.FeatureSelection.attributeSelectedClassifierWithInfoGain(wbcDataWhole, n);
        Evaluation sonarInfoEval = comp422.weka.FeatureSelection.attributeSelectedClassifierWithInfoGain(sonarDataWhole, n);

        //classifier using selected features obtained by correlation
        Evaluation wbcCorrEval = comp422.weka.FeatureSelection.attributeSelectedClassifierWithCorrelation(wbcDataWhole, n);
        Evaluation sonarCorrEval = comp422.weka.FeatureSelection.attributeSelectedClassifierWithCorrelation(sonarDataWhole, n);

        //classifier using wrapper
        Evaluation wbcWrapperEval = comp422.weka.FeatureSelection.attributeSelectedClassifierWithWrapperNaiveBayes(wbcDataWhole, n);
        Evaluation sonarWrapperEval = comp422.weka.FeatureSelection.attributeSelectedClassifierWithWrapperNaiveBayes(sonarDataWhole, n);

    }
    protected static void outputSummary(Evaluation eval, String title){
        System.out.println(title);
        System.out.println(eval.toSummaryString("\nResults\n======\n", false));
    }
}
