import weka.classifiers.Evaluation;
import comp422.weka.TrainClassifier;
import weka.core.Instances;

import java.util.Map;

/**
 * feature selection
 */
public class FeatureSelection {
    /**
     * data paths
     */
    private static final String WBC_DATA_PATH = "../Project2-dataset/uci-datasets/wbcd.names";
    private static final String SONAR_DATA_PATH = "../Project2-dataset/uci-datasets/sonar.names";
    /**
     * selected feature number
     */
    private static final int FEATURE_NUMBER = 5;

    public static void main(String[] args) throws Exception {
        String wbcDataPath = WBC_DATA_PATH;
        String sonarDataPath = SONAR_DATA_PATH;
        int n = FEATURE_NUMBER;
        if (args.length == 2){
            wbcDataPath = args[0];
            sonarDataPath = args[1];
        }
        if(args.length == 3){
            n = Integer.parseInt(args[2]);
        }
        //load data
        Instances wbcDataWhole = TrainClassifier.loadData(wbcDataPath);
        Instances sonarDataWhole = TrainClassifier.loadData(sonarDataPath);

        //split the dataset into train and test
        Map<String, Instances> splitedWbcData = TrainClassifier.splitTrainTest(wbcDataWhole);
        Map<String, Instances> splitedSonarData = TrainClassifier.splitTrainTest(sonarDataWhole);
        Instances wbcDataTrain = splitedWbcData.get("train");
        Instances wbcDataTest = splitedWbcData.get("test");
        Instances sonarDataTtrain = splitedSonarData.get("train");
        Instances sonarDataTest = splitedSonarData.get("test");

        //classifier using all features
        Evaluation wbcEval = TrainClassifier.trainNaiveBayes(wbcDataTest);
        Evaluation sonarEval = TrainClassifier.trainNaiveBayes(sonarDataTest);
        outputSummary(wbcEval);
        outputSummary(sonarEval);

        //classifier using selected features obtained by Information gain
        Instances wbcDataSelectedByInformation= comp422.weka.FeatureSelection.selectFeaturesByInformationGain(wbcDataTrain, n, wbcDataTest);
        Instances sonarDataSelectedByInformation= comp422.weka.FeatureSelection.selectFeaturesByInformationGain(sonarDataTtrain, n, sonarDataTest);
        Evaluation wbcInfoEval = TrainClassifier.trainNaiveBayes(wbcDataSelectedByInformation);
        Evaluation sonarInfoEval = TrainClassifier.trainNaiveBayes(sonarDataSelectedByInformation);
        outputSummary(wbcInfoEval);
        outputSummary(sonarInfoEval);

        //classifier using selected features obtained by correlation
        Instances wbcDataSelectedByCorrelation  = comp422.weka.FeatureSelection.selectFeaturesByCorrelation(wbcDataTrain, n, wbcDataTest);
        Instances sonarDataSelectedByCorrelation  = comp422.weka.FeatureSelection.selectFeaturesByCorrelation(sonarDataTtrain, n, sonarDataTest);
        Evaluation wbcCorrEval = TrainClassifier.trainNaiveBayes(wbcDataSelectedByCorrelation);
        Evaluation sonarCorrEval = TrainClassifier.trainNaiveBayes(sonarDataSelectedByCorrelation);
        outputSummary(wbcCorrEval);
        outputSummary(sonarCorrEval);

        //classifier using wrapper
        Instances wbcDataSelectedByWrapper  = comp422.weka.FeatureSelection.selectFeaturesByWrapperWithNaiveBayes(wbcDataTrain, n, wbcDataTest);
        Instances sonarDataSelectedByWrapper  = comp422.weka.FeatureSelection.selectFeaturesByWrapperWithNaiveBayes(sonarDataTtrain, n, sonarDataTest);
        Evaluation wbcWrapperEval = TrainClassifier.trainNaiveBayes(wbcDataSelectedByWrapper);
        Evaluation sonarWrapperEval = TrainClassifier.trainNaiveBayes(sonarDataSelectedByWrapper);
        outputSummary(wbcWrapperEval);
        outputSummary(sonarWrapperEval);

    }
    protected static void outputSummary(Evaluation eval){
        System.out.println(eval.toSummaryString("\nResults\n======\n", false));
    }
}
