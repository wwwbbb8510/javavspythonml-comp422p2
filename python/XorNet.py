### Use multiple layer FFNN to solve XOR problem

# imports
import tensorflow as tf

# configurations
tf.logging.set_verbosity(tf.logging.INFO)
TENSOR_BOARD_DIR = 'output/xor'

# add tensorboard summaries for variable
def variable_summaries(var):
  """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
  with tf.name_scope('summaries'):
    mean = tf.reduce_mean(var)
    tf.summary.scalar('mean', mean)
    with tf.name_scope('stddev'):
      stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.summary.scalar('stddev', stddev)
    tf.summary.scalar('max', tf.reduce_max(var))
    tf.summary.scalar('min', tf.reduce_min(var))
    tf.summary.histogram('histogram', var)


# simulate data
data_x = [[0, 0], [0, 1], [1, 0], [1, 1]] # input
data_y=[[0], [1], [1], [0]] # output

# Parameters
learning_rate = 0.5
num_steps = 100000
display_step = 100

# Network Parameters
n_hidden_1 = 2 # hidden layer number of neurons
num_input =  2

# tf Graph input
X = tf.placeholder("float", [None, num_input], 'X')
Y = tf.placeholder("float", [None, 1], 'Y')

# Store layers weight & bias
weights = {}
biases = {}
# hidden layer weights and biases
with tf.name_scope('layer-h'):
    weights['h1'] = tf.Variable(tf.random_normal([num_input, n_hidden_1], stddev=0.2), 'weight_h1')
    tf.summary.histogram('weights_h1_hist', weights['h1'])
    biases['b1'] = tf.Variable(tf.random_normal([n_hidden_1]), 'bias_h1')
    tf.summary.histogram('biases_h1_hist', biases['b1'])
# output layer weights and biases
with tf.name_scope('layer-o'):
    weights['out'] = tf.Variable(tf.random_normal([n_hidden_1, 1]), 'weight_o')
    tf.summary.histogram('weights_o_hist', weights['out'])
    biases['out'] = tf.Variable(tf.random_normal([1]), 'bias_o')
    tf.summary.histogram('biases_o_hist', biases['out'])

# Create model
def neural_net(x):
    # Hidden layer
    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x, weights['h1'], name='matmul_h1'), biases['b1'], 'sum_h1'), 'activation_h1')
    # Output layer
    out_layer = tf.add(tf.matmul(layer_1, weights['out'], name='matmul_output'), biases['out'], 'sum_output')
    return out_layer

# output from the neural net
logits = neural_net(X)
# use sigmoid for binary prediction
pred = tf.nn.sigmoid(logits)

# Define loss and optimizer
# Add loss in tensorboard
with tf.name_scope('loss'):
    # loss_op = tf.losses.mean_squared_error(labels=Y, predictions=logits)
    # cross entropy loss function
    loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=Y))
tf.summary.scalar('loss', loss_op)

# Use gradient descent with back prop to train the net
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
# optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate) # advanced optimiser can also be used such as Adam
train_op = optimizer.minimize(loss_op)

# Evaluate model
pred = tf.cast(tf.greater(pred, 0.5), tf.float32)
correct_pred = tf.equal(pred, Y)
with tf.name_scope('accuracy'):
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
tf.summary.scalar('accuracy', accuracy)

# merge summary
merged = tf.summary.merge_all()

# Delete tensorboard oled files
if tf.gfile.Exists(TENSOR_BOARD_DIR):
    tf.gfile.DeleteRecursively(TENSOR_BOARD_DIR)

# Create tensorboard writer
train_writer = tf.summary.FileWriter(TENSOR_BOARD_DIR)

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# Start training
with tf.Session() as sess:
    # Run the initializer
    sess.run(init)

    # Print initial weights and biases
    print('Init weights: ', sess.run(weights, feed_dict={X: data_x,
                                                 Y: data_y}))
    print('Init biases: ', sess.run(biases, feed_dict={X: data_x,
                                                         Y: data_y}))
    # Add graph to tensor board
    train_writer.add_graph(sess.graph)

    for step in range(1, num_steps+1):
        # Run optimization op (backprop)
        sess.run(train_op, feed_dict={X: data_x, Y: data_y})
        if step % display_step == 0 or step == 1:
            # Get batch loss and accuracy
            loss, acc, summary = sess.run([loss_op, accuracy, merged], feed_dict={X: data_x,
                                                                 Y: data_y})
            train_writer.add_summary(summary, step)
            # early stopping when loss is less than 1e-6
            if loss < 1e-2:
                break
            print("Step " + str(step) + ", Loss= " + \
                  "{:.4f}".format(loss) + ", Training Accuracy= " + \
                  "{:.3f}".format(acc))

    print("Optimization Finished!")

    # Calculate accuracy
    print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={X: data_x,
                                      Y: data_y}))

    # Get the prediction
    print("Logits:", sess.run(logits, feed_dict={X: data_x,
                                                    Y: data_y}))
    print("Predictions:", sess.run(pred, feed_dict={X: data_x,
                                  Y: data_y}))

    # Output the structure
    print("Weights:", sess.run(weights, feed_dict={X: data_x,
                                                      Y: data_y}))
    print("Biases:", sess.run(biases, feed_dict={X: data_x,
                                                   Y: data_y}))