
# coding: utf-8

# ## global imports and configurations

# In[188]:

# imports
import pandas as pd
import os
import numpy as np
import math
import graphviz 

from copy import copy
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LinearRegression
from sklearn import tree
from sklearn.model_selection import cross_val_score
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split


# In[136]:

# global configs
pd.set_option('display.max_columns', 30)


# ## load data and extract the complete data

# In[137]:

df_data = pd.read_csv('../Project2-dataset/uci-datasets/hepatitis.data', header=None)
df_data_header = ['Class', 'AGE', 'SEX', 'STEROID', 'ANTIVIRALS', 'FATIGUE', 'MALAISE',
                  'ANOREXIA', 'LIVERBIG', 'LIVERFIRM', 'SPLEENPALPABLE', 'SPIDERS', 'ASCITES', 'VARICES', 'BILIRUBIN', 
                 'ALKPHOSPHATE', 'SGOT', 'ALBUMIN', 'PROTIME', 'HISTOLOGY']


# In[138]:

print(df_data.shape)
print(df_data.describe(include='all'))
print(df_data.head())
print(len(df_data_header))


# In[139]:

complete_data_indice = df_data.apply(lambda row: row=='?', axis=1)
complete_data_indice = complete_data_indice.any(axis=1)
df_data_complete = df_data[-complete_data_indice]
df_data_incomplete = df_data[complete_data_indice]
print(df_data_complete.shape)
print(df_data_incomplete.shape)
print(df_data_complete.describe(include='all'))


# ## apply decision tree on the complete data by 10 fold cross-validation
# 

# In[140]:

def assess_model(data, random_state=0, cv=10):
    clf = tree.DecisionTreeClassifier(random_state=0)
    dt_train_X = data.drop(0, axis=1)
    dt_train_y = data.loc[:, 0]
    cv_results = cross_val_score(clf, dt_train_X, dt_train_y, cv=10)
    return np.mean(cv_results)


# In[141]:

assess_model(df_data_complete)


# ## analyse and fill missing data

# In[142]:

# SEX: male, female
df_data[df_data[3] == '?']


# In[143]:

# delete data with too many missing columns
def remove_rows_with_little_info(target_data, missing_col_threshold=10, missing_value_rep='?'):
    for row_index, row_series in target_data.iterrows():
        row_missing_series = row_series[row_series==missing_value_rep]
        if row_missing_series.size >= missing_col_threshold:
            target_data = target_data.drop(row_index, axis=0)
            print('row:%d has been removed' % row_index)
    return target_data

# fill categorical missing value by knn
def fill_missing_data_by_knn(filling_cols, complete_data, target_data, missing_col_threshold=3, missing_value_rep='?'):
    knn = KNeighborsClassifier(n_neighbors=3)
    for row_index, row_series in target_data.iterrows():
        row_missing_series = row_series[row_series==missing_value_rep]
        if row_missing_series.size > 0 and row_missing_series.size <= missing_col_threshold:
            df_train_X = df_data_complete.drop(row_missing_series.index.values, axis=1)
            for missing_col in filling_cols:
                if missing_col in row_missing_series.index.values:
                    df_train_y = df_data_complete.iloc[:, missing_col]
                    knn.fit(df_train_X, df_train_y)
                    ss_test_X = row_series.drop(row_missing_series.index.values)
                    test_y = knn.predict(ss_test_X.values.reshape((1,-1)))
                    target_data.loc[row_index, missing_col] = test_y[0]
                    print('row:%d, col:%d has been filled by knn' % (row_index, missing_col) )
    return target_data

# fill categorical missing value by logistic regression
def fill_missing_data_by_lregression(filling_cols, complete_data, target_data,
                                     missing_col_threshold=(4, 9), missing_value_rep='?'):
    lr = LogisticRegression()
    for row_index, row_series in target_data.iterrows():
        row_missing_series = row_series[row_series==missing_value_rep]
        if row_missing_series.size >= missing_col_threshold[0] and row_missing_series.size <= missing_col_threshold[1]:
            df_train_X = df_data_complete.drop(row_missing_series.index.values, axis=1)
            for missing_col in filling_cols:
                if missing_col in row_missing_series.index.values:
                    df_train_y = df_data_complete.iloc[:, missing_col]
                    lr.fit(df_train_X, df_train_y)
                    ss_test_X = row_series.drop(row_missing_series.index.values)
                    test_y = lr.predict(ss_test_X.values.reshape((1,-1)))
                    target_data.loc[row_index, missing_col] = test_y[0]
                    print('row:%d, col:%d has been filled by logistic regression' % (row_index, missing_col) )
    return target_data

# fill continous missing value by linear regression
def fill_missing_data_by_linearregression(filling_cols, complete_data, target_data,
                                     missing_col_threshold=(1, 9), missing_value_rep='?'):
    lr = LinearRegression()
    for row_index, row_series in target_data.iterrows():
        row_missing_series = row_series[row_series==missing_value_rep]
        if row_missing_series.size >= missing_col_threshold[0] and row_missing_series.size <= missing_col_threshold[1]:
            df_train_X = df_data_complete.drop(row_missing_series.index.values, axis=1)
            for missing_col in filling_cols:
                if missing_col in row_missing_series.index.values:
                    df_train_y = df_data_complete.iloc[:, missing_col]
                    lr.fit(df_train_X, df_train_y)
                    ss_test_X = row_series.drop(row_missing_series.index.values)
                    test_y = lr.predict(ss_test_X.values.reshape((1,-1)))
                    target_data.loc[row_index, missing_col] = test_y[0]
                    print('row:%d, col:%d has been filled by linear regression' % (row_index, missing_col) )
    return target_data


# In[144]:

# using hot deck imputation idea and Machine learning method KNN
df_data[df_data[3] == '?']


# In[145]:

# STEROID: no, yes
df_data[df_data[5] == '?']


# In[146]:

# ANTIVIRALS: no, yes
df_data[df_data[6] == '?']


# In[147]:

# FATIGUE: no, yes
df_data[df_data[7] == '?']


# In[148]:

# delete this row with too many missing columns 
df_data = remove_rows_with_little_info(df_data)
df_data.shape


# In[149]:

# MALAISE: no, yes
df_data[df_data[8] == '?']


# In[150]:

# ANOREXIA: no, yes
df_data[df_data[9] == '?']


# In[151]:

# LIVER BIG: no, yes
df_data[df_data[10] == '?']


# In[152]:

# LIVER FIRM: no, yes
df_data[df_data[11] == '?']


# In[153]:

# SPLEEN PALPABLE: no, yes
df_data[df_data[12] == '?']


# In[154]:

# SPIDERS: no, yes
df_data[df_data[13] == '?']


# In[155]:

# ASCITES: no, yes
df_data[df_data[14] == '?']


# In[156]:

# VARICES: no, yes
df_data[df_data[15] == '?']


# In[157]:

# fix categorial rows with no more than 3 missing columns with knn
filling_cols = list(range(16))
df_data = fill_missing_data_by_knn(filling_cols=filling_cols, complete_data=df_data_complete, target_data=df_data)


# In[158]:

# fix categorical rows with more than 3 and less than 10 missing columns 
df_data = fill_missing_data_by_lregression(filling_cols=filling_cols, complete_data=df_data_complete, target_data=df_data)


# In[159]:

# BILIRUBIN: 0.39, 0.80, 1.20, 2.00, 3.00, 4.00
df_data[df_data[16] == '?']


# In[160]:

# ALK PHOSPHATE: 33, 80, 120, 160, 200, 250
df_data[df_data[17] == '?']


# In[161]:

# SGOT: 13, 100, 200, 300, 400, 500, 
df_data[df_data[18] == '?']


# In[162]:

# fix continous data by linear regression
filling_cols = list(range(15, 19))
df_data = fill_missing_data_by_linearregression(filling_cols=filling_cols, complete_data=df_data_complete, target_data=df_data)


# ## apply decision tree on the filled data by 10 fold cv

# In[163]:

df_data.to_csv(path_or_buf='hepatitis_fm.data', header=df_data_header, index=False)
assess_model(df_data)


# ## analyse and descretise continous data 

# In[164]:

def convert_string_float(df_data, cols):
    for i in cols:
        df_data[i] = df_data[i].astype(float)
    return df_data
continous_cols = list(range(14, 19))
df_data = convert_string_float(df_data, continous_cols)
# backup filled data
df_data_backup = copy(df_data)


# In[165]:

def calculate_entropy(df_data, class_colum=0, class_labels=[1,2]):
    class_series = df_data[class_colum]
    total_number = class_series.size
    entropy = 0
    if total_number == 0:
        return 1
    for label in class_labels:
        p_c = sum(class_series == label) / total_number
        if p_c != 0:
            entropy -= p_c * math.log2(p_c)
    return entropy

def calculate_net_entropy(df_data, split_point, split_column, class_colum=0, class_labels=[1,2]):
    df_data_s1 = df_data.loc[df_data[split_column] < split_point]
    df_data_s2 = df_data.loc[df_data[split_column] >= split_point]
    entropy_s1 = calculate_entropy(df_data_s1)
    entropy_s2 = calculate_entropy(df_data_s2)
    net_entropy = df_data_s1.shape[0]/df_data.shape[0] * entropy_s1 + df_data_s2.shape[0]/df_data.shape[0] * entropy_s2
    return net_entropy

def search_split_point(df_data, split_points, split_column, class_colum=0, class_labels=[1,2], debug=True):
    current_max_gain = 0
    current_max_point = split_points[0]
    data_entropy = calculate_entropy(df_data)
    for point in split_points:
        net_entropy = calculate_net_entropy(df_data, point, split_column)
        new_info_gain = data_entropy - net_entropy
        if debug:
            print('net entropy:%f, data entropy:%f' % (net_entropy, data_entropy))
            print('current point:%f, current info gain:%f' % (point, new_info_gain))
        if new_info_gain > current_max_gain:
            current_max_point = point
            current_max_gain = new_info_gain
    if debug:
        if current_max_gain == 0:
            print('zero gain point found - point:%f, info-gain:%f' % (current_max_point, current_max_gain))
        else:
            print('split point found - point:%f, info-gain:%f' % (current_max_point, current_max_gain))
    return (current_max_point, current_max_gain)

def get_all_split_points(data, split_points, split_column, step_num, class_colum=0, class_labels=[1,2]):
    return_spoint_points = []
    cloned_data = copy(data)
    if step_num > 0:
        point, gain = search_split_point(cloned_data, split_points, split_column, class_colum, class_labels)
        if gain == 0:
            return return_spoint_points
        else:
            return_spoint_points = np.append(return_spoint_points, point)
            sub_split_points = [ split_points[split_points<point], split_points[split_points>=point] ]
            sub_data = [ cloned_data[cloned_data[split_column]<point], cloned_data[cloned_data[split_column]>=point] ]
            new_step_num = step_num - 1
            for i in range(len(sub_data)):
                if len(sub_split_points[i]) > 0:
                    sub_points = get_all_split_points(sub_data[i], sub_split_points[i], split_column, new_step_num, class_colum, class_labels)
                    return_spoint_points = np.append(return_spoint_points, sub_points)
    return return_spoint_points

def discretise_data(data, split_num, split_column, step_num, class_colum=0, class_labels=[1,2], unique_threshold=10):
    cloned_data = copy(data)
    if pd.Series.unique(cloned_data[split_column]).size > unique_threshold:
        split_points = np.linspace(cloned_data[split_column].min(), cloned_data[split_column].max(), split_num+1, False)
        split_points = np.delete(split_points, 0)
        split_points = get_all_split_points(cloned_data, split_points, split_column, step_num, class_colum, class_labels)
        split_points = np.sort(split_points)
        print(split_points)
        cloned_colum_data = copy(cloned_data[split_column])
        for i in range(len(split_points)):
            if i==0: 
                cloned_data.loc[cloned_colum_data < split_points[i], split_column] = i+1
            else:
                cloned_data.loc[np.logical_and(cloned_colum_data >= split_points[i-1], cloned_colum_data < split_points[i]), split_column ] = i+1
        cloned_data.loc[cloned_colum_data >= split_points[split_points.size-1], split_column] = split_points.size
    return cloned_data


# In[166]:

def plot_hist_column(col, df_data=df_data):
    print(df_data[col].describe())
    plt.figure()
    plt.hist(df_data[col])
    plt.show()


# In[167]:

plot_hist_column(1, df_data)


# In[168]:

# discretise column 1
df_data = discretise_data(df_data, 20, 1, 3)
plot_hist_column(1, df_data)


# In[169]:

assess_model(df_data)


# In[170]:

plot_hist_column(14, df_data)


# In[171]:

# discretise column 14 which doesn't help
# df_data = discretise_data(df_data, 20, 14, 4)
# plot_hist_column(14, df_data)


# In[172]:

assess_model(df_data)


# In[173]:

plot_hist_column(15, df_data)


# In[174]:

# discretise column 15 which doesn't help
# df_data = discretise_data(df_data, 20, 15, 3)
# plot_hist_column(15, df_data)


# In[175]:

# df_data = df_data_backup
assess_model(df_data)


# In[176]:

plot_hist_column(16, df_data)


# In[177]:

# discretise column 16
df_data = discretise_data(df_data, 20, 16, 4)
plot_hist_column(16, df_data)


# In[178]:

assess_model(df_data)


# In[179]:

plot_hist_column(17, df_data)


# In[180]:

# discretise column 17 which doesn't help
# df_data = discretise_data(df_data, 20, 17, 4)
# plot_hist_column(17, df_data)


# In[181]:

# df_data = df_data_backup
assess_model(df_data)


# In[182]:

plot_hist_column(18, df_data)


# In[183]:

# discretise column 18 which doesn't help
# df_data = discretise_data(df_data, 20, 18, 4)
# plot_hist_column(18, df_data)


# In[184]:

# df_data = df_data_backup
df_data.to_csv(path_or_buf='hepatitis_dz.data', header=df_data_header, index=False)
assess_model(df_data)


# ## apply decision tree after discretisation

# In[185]:

df_data.columns = df_data_header
df_data.head()


# In[194]:

clf = tree.DecisionTreeClassifier(random_state=0)

dt_X = df_data.drop('Class', axis=1)
dt_y = df_data.loc[:, 'Class']
dt_train_X, dt_test_X, dt_train_y, dt_test_y = train_test_split(dt_X, dt_y, test_size=0.1, random_state=42)
print(dt_train_X.shape)
print(dt_test_X.shape)
clf.fit(dt_train_X, dt_train_y)
training_accuracy = clf.score(dt_train_X, dt_train_y)
test_accuracy = clf.score(dt_test_X, dt_test_y)
print(training_accuracy, test_accuracy)
# cv_results = cross_val_score(clf, dt_train_X, dt_train_y, cv=10)
# print(cv_results)
# np.mean(cv_results)


# In[187]:

tree.export_graphviz(clf, out_file='dp_tree.dot', feature_names=dt_train_X.columns.values) 
dot_data = tree.export_graphviz(clf, out_file=None, feature_names=dt_train_X.columns.values) 
graph = graphviz.Source(dot_data) 
graph


# In[ ]:



