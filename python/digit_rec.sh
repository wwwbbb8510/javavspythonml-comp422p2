#!/bin/bash
# for i in 0 1 2 3 4 5 6 7 8
for i in 0 3 5 8
do
    python DigitRecognition.py $i
done        

# evaluate only
# python DigitRecognition.py 8 test

# tensorboard commands
# tensorboard --logdir=tmp/digits00_convnet_model   #0
# tensorboard --logdir=tmp/digits05_convnet_model   #1
# tensorboard --logdir=tmp/digits10_convnet_model   #2
# tensorboard --logdir=tmp/digits15_convnet_model   #3
# tensorboard --logdir=tmp/digits20_convnet_model   #4
# tensorboard --logdir=tmp/digits30_convnet_model   #5
# tensorboard --logdir=tmp/digits40_convnet_model   #6
# tensorboard --logdir=tmp/digits50_convnet_model   #7
# tensorboard --logdir=tmp/digits60_convnet_model   #8

# tensorboard --logdir=tmp/digits00_convnet_model   #0
# tensorboard --logdir=tmp/digits15_convnet_model   #3
# tensorboard --logdir=tmp/digits30_convnet_model   #5
# tensorboard --logdir=tmp/digits60_convnet_model   #8