# imports
import random
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.python.framework import ops
import pandas as pd
from sys import argv
from sklearn.model_selection import train_test_split

#configs
DIGITS_FILE_PATH = '../Project2-dataset/digits/digits'


def main(argv):
    print('[%s]' % ', '.join(map(str, argv)))
    # Load training and eval data
    # suffix options - 9 in total
    suffix_options = ['00', '05', '10', '15', '20', '30', '40', '50', '60']
    suffix_index = int(argv[1]) if len(argv) == 2 else 0;   # change the index from 0 to 8 for different data sets
    suffix = suffix_options[suffix_index]
    X_train, X_test, y_train, y_test = load_data(suffix)

    ops.reset_default_graph()
    # Create graph
    sess = tf.Session()

    # Declare k-value and batch size
    k = 1
    if suffix_index == 8:
        k = 21
    elif suffix_index == 5:
        k = 21
    elif suffix_index == 3:
        k = 11
    batch_size = 10

    # Placeholders
    x_data_train = tf.placeholder(shape=[None, 49], dtype=tf.float32)
    x_data_test = tf.placeholder(shape=[None, 49], dtype=tf.float32)
    y_target_train = tf.placeholder(shape=[None], dtype=tf.float32)
    y_target_test = tf.placeholder(shape=[None], dtype=tf.float32)

    # transform y value to one-hot vector
    y_target_hot_train = tf.one_hot(indices=tf.cast(y_target_train, tf.int32), depth=10)
    y_target_hot_test = tf.one_hot(indices=tf.cast(y_target_test, tf.int32), depth=10)

    # Declare distance metric
    # L1
    # Broadcasting: N * 49 - B_size * 1 * 49 = B_size * N * 49
    #               => B_size * N
    distance = tf.reduce_sum(tf.abs(tf.subtract(x_data_train, tf.expand_dims(x_data_test, 1))), axis=2)

    # L2
    # distance = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(x_data_train, tf.expand_dims(x_data_test,1))), reduction_indices=1))

    # Predict: Get min distance index (Nearest neighbor)
    top_k_xvals, top_k_indices = tf.nn.top_k(tf.negative(distance), k=k) # B_size * k
    prediction_indices = tf.gather(y_target_hot_train, top_k_indices)  # B_size * k
    # Predict the mode category
    count_of_predictions = tf.reduce_sum(prediction_indices, axis=1) # B_size * 1
    prediction = tf.argmax(count_of_predictions, axis=1)

    # Calculate how many loops over training data
    num_loops = int(np.ceil(len(X_test) / batch_size))

    # run k means 10 times
    accuracy_10 = []
    for i in range(10):
        X_train, X_test, y_train, y_test = load_data(suffix)
        test_output = []
        actual_vals = []
        for i in range(num_loops):
            min_index = i * batch_size
            max_index = min((i + 1) * batch_size, len(X_train))
            x_batch = X_test[min_index:max_index]
            y_batch = y_test[min_index:max_index]
            predictions = sess.run(prediction, feed_dict={x_data_train: X_train, x_data_test: x_batch,
                                                          y_target_train: y_train, y_target_test: y_batch})
            test_output.extend(predictions)
            actual_vals.extend(y_batch)
        test_size = len(X_test)
        accuracy = sum([1 for i in range(test_size) if test_output[i] == actual_vals[i]]) / test_size
        accuracy_10.append(accuracy)
        print('Accuracy on test set: ' + str(accuracy))
    print(accuracy_10)
    print('Average accuracy of 10 runs: %f' % (sum(accuracy_10)/10))

# load data from digits file
def load_data(suffix='00'):
    full_path = DIGITS_FILE_PATH + suffix
    print('Reading data from ' + full_path)
    df_data = pd.read_csv(full_path, delim_whitespace=True, header=None)
    df_X = df_data.iloc[:, 0:-1]
    df_y = df_data.iloc[:, -1]
    X_train, X_test, y_train, y_test = train_test_split(df_X.as_matrix(), df_y.as_matrix(),
                                                        test_size = 0.5)
    X_train = np.asarray(X_train, dtype=np.float32)
    X_test = np.asarray(X_test, dtype=np.float32)
    y_train = np.asarray(y_train, dtype=np.int32)
    y_test = np.asarray(y_test, dtype=np.int32)
    return X_train, X_test, y_train, y_test

if __name__ == "__main__":
    main(argv)