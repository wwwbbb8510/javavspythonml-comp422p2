# imports
import tensorflow as tf
import numpy as np
import sklearn as skl
from sklearn.model_selection import train_test_split
import pandas as pd
import sys
import math

# configurations
tf.logging.set_verbosity(tf.logging.INFO)
DIGITS_FILE_PATH = '../Project2-dataset/digits/digits' # image folder
TRAINING_STEPS = 20000 # total training step
EVALUATION_STEPS = 100 # evaluation step
ENABLE_TRAINING_HOOK = False # enable training hook which is for logging
learning_rate = 0.01 # learning rate
regularizer_rates = [0.1, 0.1, 0.1] # regularizer rates for each layer
dropout_rate = 0.4 # dropout rate
# early stopping criteria
early_stoping_test_loss = 1e-6 # early stopping criterion of test loss
early_stop_ascending_epoch = 10 # early stopping crierion of asending epoch
# net structure
size_at_layers = [4, 4, 32] # the size of each layer
# size_at_layers = [16, 32, 256]

# main entry
def main(argv):
    print('[%s]' % ', '.join(map(str, argv)))
    # Load training and eval data
    # suffix options - 9 in total
    suffix_options = ['00', '05', '10', '15', '20', '30', '40', '50', '60']
    suffix_index = int(argv[1]) if len(argv) >= 2 else 0   # change the index from 0 to 8 for different data sets
    suffix = suffix_options[suffix_index]
    X_train, X_test, y_train, y_test = load_data(suffix)

    # parse running_mode from parameters
    running_mode = argv[2] if len(argv) >= 3 else 'train'

    # set parameters related to data type
    global learning_rate, regularizer_rates, dropout_rate, early_stoping_test_loss, size_at_layers
    if suffix_index == 8:
        size_at_layers = [32, 64, 512]
        regularizer_rates = [0.8, 0.8, 0.8]
        learning_rate = 0.0001
    elif suffix_index == 5:
        size_at_layers = [16, 16, 128]
        regularizer_rates = [0.5, 0.5, 0.5]
        learning_rate = 0.0001
    elif suffix_index == 3:
        size_at_layers = [8, 8, 64]
        regularizer_rates = [0.3, 0.3, 0.3]
        learning_rate = 0.0001


    # Create the Estimator
    run_config = tf.estimator.RunConfig().replace(
        save_summary_steps = 100
    )
    digits_classifier = tf.estimator.Estimator(
        model_fn= cnn_digit_recognition_model_fn, model_dir='output/digits' + suffix + '_convnet_model', \
        config=run_config)

    # Set up logging for predictions
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=100)

    # set training hooks
    hooks = []
    if ENABLE_TRAINING_HOOK:
        hooks.append(logging_hook)

    # Train and evaluate inputs
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": X_train},
        y=y_train,
        batch_size=500,
        num_epochs=None,
        shuffle=True)
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": X_test},
        y=y_test,
        num_epochs=1,
        shuffle=False
    )

    # Train the model
    if running_mode == 'train':
        ascending_epoch = 0
        previous_loss = 10000
        for epoch in range(math.ceil(TRAINING_STEPS/EVALUATION_STEPS)):
            digits_classifier.train(
                input_fn=train_input_fn,
                steps=EVALUATION_STEPS,
                hooks=hooks)
            eval_results = digits_classifier.evaluate(input_fn=eval_input_fn)
            print(eval_results)
            if eval_results['loss'] < early_stoping_test_loss:
                print('early stopping at loss of %f' % eval_results['loss'])
                break
            if early_stop_ascending_epoch > 0:
                if eval_results['loss'] > previous_loss:
                    ascending_epoch = ascending_epoch + 1
                else:
                    ascending_epoch = 0
                print('continuous loss ascending epoch: %d' % ascending_epoch)
                previous_loss = eval_results['loss']
                if ascending_epoch > early_stop_ascending_epoch:
                    print('early stopping when evaluate loss continue growing ten epoch')
                    break


    # Evaluate the model and print results
    eval_results = digits_classifier.evaluate(input_fn=eval_input_fn)
    print(eval_results)


# load data from digits file
def load_data(suffix='00'):
    full_path = DIGITS_FILE_PATH + suffix
    print('Reading data from ' + full_path)
    df_data = pd.read_csv(full_path, delim_whitespace=True, header=None)
    df_X = df_data.iloc[:, 0:-1]
    df_y = df_data.iloc[:, -1]
    X_train, X_test, y_train, y_test = train_test_split(df_X.as_matrix(), df_y.as_matrix(),
                                                        test_size = 0.5)
    X_train = np.asarray(X_train, dtype=np.float32)
    X_test = np.asarray(X_test, dtype=np.float32)
    y_train = np.asarray(y_train, dtype=np.int32)
    y_test = np.asarray(y_test, dtype=np.int32)
    return X_train, X_test, y_train, y_test

# build cnn model for digit recognition
def cnn_digit_recognition_model_fn(features, labels, mode):
    # Input Layer
    # input image is 7*7
    input_layer = tf.reshape(features["x"], [-1, 7, 7, 1])

    # Convolutional Layer #1
    # apply C1 3*3 filter and output will be [batch_size, 7, 7, C1]
    conv1 = tf.layers.conv2d(
        inputs=input_layer,
        filters=size_at_layers[0],
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        kernel_regularizer= tf.contrib.layers.l2_regularizer(regularizer_rates[0])
    )

    # Pooling Layer #1
    # apply 2*2 filter and stride of 1, and output will be [batch_size, 7, 7, C1]
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=1, padding='same')

    # Convolutional Layer #2
    # apply C2 3*3 filter and output will be [batch_size, 7, 7, C2]
    conv2 = tf.layers.conv2d(
        inputs=pool1,
        filters=size_at_layers[1],
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu,
        kernel_regularizer= tf.contrib.layers.l2_regularizer(regularizer_rates[1])
    )

    # Pooling Layer #2
    # apply 2*2 filter and stride of 2, and output will be [batch_size, 4, 4, C2]
    pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2, padding='same' )

    # Dense Layer
    pool2_flat = tf.reshape(pool2, [-1, 4 * 4 * size_at_layers[1]])
    dense = tf.layers.dense(inputs=pool2_flat, units=size_at_layers[2], activation=tf.nn.relu,
        kernel_regularizer= tf.contrib.layers.l2_regularizer(regularizer_rates[2]))

    dropout = tf.layers.dropout(
        inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN)

    # logits Layer
    logits = tf.layers.dense(inputs=dropout, units=10, kernel_regularizer=tf.contrib.layers.l2_regularizer(regularizer_rates[2]))

    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "classes": tf.argmax(input=logits, axis=1),
        # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
        "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss (for both TRAIN and EVAL modes)
    onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=10)
    loss = tf.losses.softmax_cross_entropy(
        onehot_labels=onehot_labels, logits=logits)

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        # optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

if __name__ == "__main__":
    tf.app.run(argv=sys.argv)
